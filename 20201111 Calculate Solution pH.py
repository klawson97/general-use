# -*- coding: utf-8 -*-
"""
Created on Tue Nov 10 20:24:33 2020

@author: klawson
"""

# NH4: charge +1, equilibrium constant = [NH4+]/[NH3][H+], pK = -9.23374
# HMPA: charge +1, equilibrium constant =  [HMPA+]/[MPA][H], pK = -9.883
# HETA: charge +1, equilibrium constant = [HETA+]/[ETA][H]. pK -9.4638

# Let’s check the functionality of the code with a full factorial of 0.01 M inputs:

# NH4 = 0, HMPA = 0, HETA = 0
# NH4 = 0, HMPA = 0, HETA = 0.01
# NH4 = 0, HMPA = 0.01, HETA = 0
# NH4 = 0, HMPA = 0.01, HETA = 0.01
# NH4 = 0.01, HMPA = 0, HETA = 0
# NH4 = 0.01, HMPA = 0, HETA = 0.01
# NH4 = 0.01, HMPA = 0.01, HETA = 0
# NH4 = 0.01, HMPA = 0.01, HETA = 0.01



def findpH_threemolecules(df_conc, pKa1, charge1,pKa2, charge2,pKa3,charge3):
# finding pH using pHcalc package

    from pHcalc.pHcalc import Acid, Neutral, System
    import numpy as np
    import pandas as pd

    df_mol1 = df_conc.iloc[1]
    df_mol2 = df_conc.iloc[2]
    df_mol3 = df_conc.iloc[3]
    
    for index, row in df_conc.iterrows(): 
        
        mol1 = Acid(pKa = pKa1, charge = charge1, conc = df_mol1.iloc[index])
        mol2 = Acid(pKa = pKa2, charge = charge2, conc = df_mol2.iloc[index])
        mol3 = Acid(pKa = pKa3, charge = charge3, conc = df_mol3.iloc[index])
        system = System(mol1,mol2,mol3)
        df_conc['pH'].iloc[index] = system.pHsolve()
        
    return

#specific example


    # conc_data = {'nh4_conc': [0,0,0,0,0.01,0.01,0.01,0.01],
    #              'hmpa_conc': [0,0,0.01,0.01,0,0,0.01,0.01],
    #              'heta_conc':[0,0.01,0,0.01,0,0.01,0,0.01]
    #              }
    # df_conc = pd.DataFrame(conc_data)
    
    # nh4_pK = -9.23374
    # hmpa_pK = -9.883
    # heta_pK = -9.4638
    # for index, row in df_conc.iterrows():
    
    #     nh4 = Acid(pKa = nh4_pK, charge1 = 1, conc = df_conc['nh4_conc'].iloc[index])
    #     hmpa = Acid(pKa = hmpa_pK, charge2 = 1, conc = df_conc['hmpa_conc'].iloc[index])
    #     heta = Acid(pKa = heta_pK, charge3 = 1, conc = df_conc['heta_conc'].iloc[index])
    #     system = System(nh4, hmpa, heta)
    #     df_conc['pH'].iloc[index] = system.pHsolve()
        
    # return


# -*- coding: utf-8 -*-
"""
Created on Tue Nov 10 20:24:33 2020

@author: klawson
"""

# NH4: charge +1, equilibrium constant = [NH4+]/[NH3][H+], pK = -9.23374
# HMPA: charge +1, equilibrium constant =  [HMPA+]/[MPA][H], pK = -9.883
# HETA: charge +1, equilibrium constant = [HETA+]/[ETA][H]. pK -9.4638

# Let’s check the functionality of t(he code with a full factorial of 0.01 M inputs:

# NH4 = 0, HMPA = 0, HETA = 0
# NH4 = 0, HMPA = 0, HETA = 0.01
# NH4 = 0, HMPA = 0.01, HETA = 0
# NH4 = 0, HMPA = 0.01, HETA = 0.01
# NH4 = 0.01, HMPA = 0, HETA = 0
# NH4 = 0.01, HMPA = 0, HETA = 0.01
# NH4 = 0.01, HMPA = 0.01, HETA = 0
# NH4 = 0.01, HMPA = 0.01, HETA = 0.01

def findpH(df_pH, df_conc, pKa1, pKa2, pKa3):
# finding pH using pHcalc package

    from pHcalc.pHcalc import Acid, Neutral, System
    import numpy as np
    import pandas as pd

    
    #find [NH3], [NH4+], [H+], [OH]
    total_nh3 = 0.01
    
    #pKa values
    nh4_pK = -9.23374
    hmpa_pK = -9.883
    heta_pK = -9.4638
    water_pK = 14
    Kw = 10**(-water_pK)
    
    #guess pH to find [H+]
    pH = 10
    conc_h = 10**(-pH)
    nh4_K = 10**(-nh4_pK)
    hmpa_K = 10**(-hmpa_pK)
    heta_K = 10**(-heta_pK)
    conc_nh4 = (nh4_K*conc_h*total_nh3)/(1+(nh4_K*conc_h))
    conc_oh = Kw/conc_h
    add_hnh4 = conc_h + conc_nh4
    
    while add_hnh4 > conc_oh:
        pH = pH + .001
        conc_h = 10**(-pH)
        nh4_K = 10**(-nh4_pK)
        conc_nh4 = (nh4_K*conc_h*total_nh3)/(1+(nh4_K*conc_h))
        conc_nh3 = total_nh3 - conc_nh4
        conc_oh = Kw/conc_h
        add_hnh4 = conc_h + conc_nh4
        
    while add_hnh4 < conc_oh:
        pH = pH - .001
        conc_h = 10**(-pH)
        nh4_K = 10**(-nh4_pK)
        conc_nh4 = (nh4_K*conc_h*total_nh3)/(1+(nh4_K*conc_h))
        conc_nh3 = total_nh3 - conc_nh4
        conc_oh = Kw/conc_h
        add_hnh4 = conc_h + conc_nh4
       
    # %%
    
    from pHcalc.pHcalc import Acid, Neutral, System
    import numpy as np
    import pandas as pd
    
    

    conc_data = {'nh4_conc': [0,0,0,0,0.01,0.01,0.01,0.01],
                 'hmpa_conc': [0,0,0.01,0.01,0,0,0.01,0.01],
                 'heta_conc':[0,0.01,0,0.01,0,0.01,0,0.01]
                 }
    df_conc = pd.DataFrame(conc_data)
    
    #empty dataframe for pH values
    df_pH=[]
    nh4_pK = -9.23374
    hmpa_pK = -9.883
    heta_pK = -9.4638

    for index, row in df_conc.iterrows():
    
        nh4 = Acid(pKa = -nh4_pK, charge = 1, conc = df_conc['nh4_conc'].iloc[index])
        hmpa = Acid(pKa = -hmpa_pK, charge = 1, conc = df_conc['hmpa_conc'].iloc[index])
        heta = Acid(pKa = -heta_pK, charge = 1, conc = df_conc['heta_conc'].iloc[index])
        system = System(nh4,hmpa,heta)
        system.pHsolve(guess_est=True)
        df_pH.append(system.pH)

    df_pH = pd.DataFrame(df_pH, columns=['pH'])
    df_conc = df_conc.join(df_pH)
    return
    


# %%
