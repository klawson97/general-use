# -*- coding: utf-8 -*-
"""
Created on Mon Sep 12 04:57:22 2022

@author: klawson
"""

'''
WARNING!!!!!!!!!!!!!!!!!!!!!
CODE NOT FINISHED!!!!!!!!!

COULD NOT FIGURE OUT ISSUE WITH WESTERN EUROPEAN WINDOWS FORMAT
INSTEAD OF UTF-8
'''


import pandas as pd
import datetime
import io

def filterdatetime(xlsxfile,outputfile,startdate,enddate):
    
    '''
    There are a lot of datapoints in this text file. 
    I am only interested in a subset of the dates within this file.
    The process temp is in deg F.
    '''
    #import text file and create DataFrame

    df_temp = pd.read_excel(xlsxfile,
                          skiprows = 6)
    
    df_temp.rename(columns = {'Date and Time':'DateTime'})
    
    #find timepoints on 9/7/2022 only

    df_temp['DateTime'] = df_temp['DateTime'] > startdate
    df_temp['DateTime'] = df_temp['DateTime'] < enddate
    
    #convert to Excel file

    writer = pd.ExcelWriter(outputfile)
    df_temp.to_excel(writer,sheet_name='Temp')
    
    return df_temp

xlsxfile = 'M:/109-2205 DEHA Volatility/Pre-test Calculations/Temperature Rate/HP AC DAQ 08.31.22 15.00.38.xlsx'
outputfile = 'M:/109-2205 DEHA Volatility/Pre-test Calculations/Temperature Rate/HP AC DAQ 09.07.22.xlsx'
startdate = datetime.datetime(2022,9,6)
enddate = datetime.datetime(2022,9,8)

filter = filterdatetime(xlsxfile,outputfile,startdate,enddate)
    