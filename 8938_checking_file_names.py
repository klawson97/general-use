# -*- coding: utf-8 -*-
"""
Created on Tue Sep 28 13:39:35 2021

@author: klawson
"""

import pandas as pd
import xlsxwriter
import os

def list_file_names(input_folder, output_folder, output_file, sheetname):

    filenames = []
    fullfilename = []
    
        #import all Excel files
    for (root, dirs, files) in os.walk(input_folder):
        for name in files:
            remove = ["Scrape Sample Traveler (", ").docx"]
            for word in remove:
                name = name.replace(word, "")
            print(name)
            filenames.append(name)
            WorkingFile = os.path.join(root,name)
            fullfilename.append(WorkingFile)
            df_pics = pd.DataFrame(list(zip(fullfilename,filenames)), columns=['Full Directory Address','Scrape Name'])
     # save output
    pic_file = output_folder +'/'+ output_file
    writer = pd.ExcelWriter(pic_file, engine='xlsxwriter')
    df_pics.to_excel(writer, sheet_name=sheetname)
    writer.save()
   
input_folder = r'M:\89-38 Bruce Calandria Duct Cleaning\B2171 Scrape Campaign\Travelers\Scrape-Specific\2022-04-29 Travelers\_From Schreiber, Daniel K\2022-04-28 - Copy'
sheetname = 'Names'
output_folder =  r'M:\89-38 Bruce Calandria Duct Cleaning\B2171 Scrape Campaign\Travelers\Scrape-Specific\2022-04-29 Travelers'
output_file = 'Confirm Receipt of Travelers 2022-04-29.xlsx'
list_file_names(input_folder, output_folder, output_file, sheetname)

#add 'New Name' Column to Excel file and list the new name you want to add to each file name
#save file and use that file for the next function below

def change_file_names(name_file, pic_folder, sheetname):

    xl = pd.ExcelFile(name_file)
    df_names = xl.parse(sheetname)
    oldname = df_names['File Name']
    newname = df_names['New Name']
    salt = df_names['Salt']
    deposit = df_names['Salt Deposit Density']
    under = df_names['Under']
    files = os.listdir(pic_folder)
    
    for index, file in enumerate(files):
        os.rename(os.path.join(pic_folder,oldname.loc[index]), os.path.join(pic_folder,salt.loc[index]+under.loc[index]+deposit.loc[index]+under.loc[index]+newname.loc[index]+under.loc[index]+oldname.loc[index]))

# 5892 Pre-Test Photos
pic_folder = r'\\DEI\shares\Projects\58-92 Salt Dep Corr Testing\Testing\Official Tests\Test Program #3\Coupon Photos - Test Program 3\Pre-Test'
name_file = r'\\DEI\shares\Projects\58-92 Salt Dep Corr Testing\Testing\Official Tests\Test Program #3\Coupon Photos - Test Program 3\Pre-Test\5892 Pre Test 3 Photo Names v03.xlsx'
#change_file_names(name_file, pic_folder,sheetname)



