#define global functions

import  os
import ioutils as io
import shutil
from pathlib import Path
import glob
import fnmatch

# Function to create new folder if not exists
def make_new_folder(folder_name, parent_folder):
      
    # Path
    path = os.path.join(parent_folder, folder_name)
      
    # Create the folder
    # 'new_folder' in
    # parent_folder
    try: 
        # mode of the folder
        mode = 0o777
  
        # Create folder
        os.mkdir(path, mode) 
    except OSError as error: 
        print(error)


'''
I had to download pictures using 5 different zip files. Pictures that belong
in the same folder were distributed in separate folder with the same name. 
Now I have the 
parent folder (different names) - the folders that stemmed from the 5 zip files
    subfolder (same names) - the dates
        subfolder (same names) - the scrapes
'''

def mergefolderswithsamename_sublevel2(merge_folder_name, path, subdir1_list, 
                                        subdir2_list,folder_list):
    make_new_folder(merge_folder_name,path)
    merge_folder = path+'/'+merge_folder_name

    for y in subdir1_list:
        make_new_folder(y, merge_folder)
        merge_folder = merge_folder+'/'+y
        for x in subdir2_list:
            make_new_folder(x,merge_folder)
            dir_1 = merge_folder+'/'+ x
            for sub_dir2 in folder_list:
                dir_next = path+'/'+sub_dir2+'/'+y+'/'+ x
                if (os.path.isdir(dir_1)) and (os.path.isdir(dir_next)):
                    file_list = os.listdir(dir_next)
                    for item in file_list:
                        retrieve_content = dir_next + '/' + item
                        shutil.move(retrieve_content,dir_1)

'''Example for how to use function above'''                        
# file_subdir_list = r'M:\89-38 Bruce Calandria Duct Cleaning\B2171 Scrape Campaign\PNNL Characterization Results (Preliminary)\Photos With Scale - Copy\Scrape Names.xlsx'
# dfinput = io.load_excel_to_dataframe(file_subdir_list, 'Sheet1',2)
# subdir2_list = dfinput.Scrape.values.tolist()
# folder_list = ['Set 1','Set 2','Set 3','Set 4','Set 5']
# filepath = r'M:\89-38 Bruce Calandria Duct Cleaning\B2171 Scrape Campaign\PNNL Characterization Results (Preliminary)\Photos With Scale - Copy'
# date = ['5-31-2022']
# merge_name = 'All Photos'

#mergefolderswithsamename_sublevel2(merge_name,filepath, date,subdir2_list,folder_list)



def mergefolderswithsamename_sublevel1(merge_folder_name,path, subdir1_list,folder_list):
    make_new_folder(merge_folder_name,path)
    merge_folder = path+'/'+merge_folder_name
    for x in subdir1_list:
        dir_1 = merge_folder+'/'+ x
        for sub_dir in folder_list:
            dir_next = path+'/'+sub_dir+'/'+ x
            if (os.path.isdir(dir_1)) and (os.path.isdir(dir_next)):
                file_list = os.listdir(dir_next)
                for item in file_list:
                    retrieve_content = dir_next + '/' + item
                    shutil.move(retrieve_content,dir_1)
                    

def groupfileswithsameword_sublevel2(parent_path, new_folder_name, word):
    '''
    I need to group files based on a shared word(s) in each name. 
    These files need to be exported to a new subfolder.
    There are multiple folders that have this same issue. They all have the same
    parent directory so I can use a for loop to complete this task.     
    '''

#generate a list of the subdirectories in the parent directory
    subdir_list = [x[0] for x in os.walk(parent_path)]

#create a for loop that goes through each subdirectory in the parent directory
#In the loop, create a new folder (sub level 2) in each sub level 1 to place the 
#photos that have the same particular word
    for x in subdir_list:
        subdir2_path = Path(parent_path,x)
        
        #use function to make a new folder if there isn't already one to 
        #appropriately organize files
        if os.path.exists(Path(subdir2_path,new_folder_name)) == False: 
            new_folder = make_new_folder(new_folder_name, subdir2_path)
        
        #move the files with the word of interest into the new folder
        subdir2_list = os.listdir(subdir2_path)
        for p in fnmatch.filter(subdir2_list, word):
            old_folder = os.path.join(subdir2_path,p)
            new_folder = os.path.join(subdir2_path,new_folder_name,p)
            shutil.move(old_folder,new_folder)
    return




    # Parameters
    # ----------
    # merge_folder_name : TYPE
    #     DESCRIPTION.
    # path : TYPE
    #     DESCRIPTION.
    # subdir1_list : TYPE
    #     DESCRIPTION.
    # folder_list : TYPE
    #     DESCRIPTION.

    # Returns
    # -------
    # None.
