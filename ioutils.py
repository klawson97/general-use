import pandas as pd
import yaml
import numpy as np

class Struct:
    def __init__(self, **entries): 
        self.__dict__.update(entries)


def load_yaml_to_dict(yaml_file):    
    ''' Open a YAML file and load contents to a dict. '''

    with open (yaml_file, 'rb') as f:
        d = yaml.load(f)
    
    return d


def load_yaml_to_struct(yaml_file):    
    ''' Open a YAML file and load contents to a struct. '''
    with open (yaml_file, 'rb') as f:
        d = yaml.load(f)
        
    s = Struct(**d)  
    
    return s


def load_excel_to_dataframe(excel_file,sheet_name,num_columns):
    ''' Open an Excel file and import the contents into a 
        pandas DataFrame.
    '''

    xls = pd.ExcelFile(excel_file)
    df = xls.parse(
        sheet_name, index_col=None, 
        parse_cols= list(np.arange(0,num_columns,1))
        )

    return df
    
def list_duplicates_of(seq,item):
    start_at = -1
    locs = []
    while True:
        try:
            loc = seq.index(item,start_at+1)
        except ValueError:
            break
        else:
            locs.append(loc)
            start_at = loc
    return locs